# Polish translation
# Copyright (C) 2021 VideoLAN
# This file is distributed under the same license as the vlc package.
#
# Translators:
# Grzegorz Pruchniakowski <gootector@o2.pl>, 2013
# Michał Trzebiatowski <michtrz@gmail.com>, 2012-2017
msgid ""
msgstr ""
"Project-Id-Version: VideoLAN's websites\n"
"Report-Msgid-Bugs-To: vlc-devel@videolan.org\n"
"POT-Creation-Date: 2020-02-05 18:13+0100\n"
"PO-Revision-Date: 2017-12-09 20:37+0100\n"
"Last-Translator: Michał Trzebiatowski <michtrz@gmail.com>, 2017\n"
"Language-Team: Polish (http://www.transifex.com/yaron/vlc-trans/language/"
"pl/)\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n"
"%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n"
"%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);\n"

#: include/header.php:289
msgid "a project and a"
msgstr "projekt i"

#: include/header.php:289
msgid "non-profit organization"
msgstr "organizacja non-profit"

#: include/header.php:298 include/footer.php:79
msgid "Partners"
msgstr "Partnerzy"

#: include/menus.php:32
msgid "Team &amp; Organization"
msgstr "Zespół i Organizacja"

#: include/menus.php:33
msgid "Consulting Services &amp; Partners"
msgstr "Usługi doradcze i Partnerzy"

#: include/menus.php:34 include/footer.php:82
msgid "Events"
msgstr "Wydarzenia"

#: include/menus.php:35 include/footer.php:77 include/footer.php:111
msgid "Legal"
msgstr "Licencja"

#: include/menus.php:36 include/footer.php:81
msgid "Press center"
msgstr "Dla mediów"

#: include/menus.php:37 include/footer.php:78
msgid "Contact us"
msgstr "Kontakt"

#: include/menus.php:43 include/os-specific.php:279
msgid "Download"
msgstr "Pobierz"

#: include/menus.php:44 include/footer.php:35
msgid "Features"
msgstr "Funkcjonalność"

#: include/menus.php:45 vlc/index.php:57 vlc/index.php:63
msgid "Customize"
msgstr "Dostosuj"

#: include/menus.php:47 include/footer.php:69
msgid "Get Goodies"
msgstr "Gadżety"

#: include/menus.php:51
msgid "Projects"
msgstr "Projekty"

#: include/menus.php:71 include/footer.php:41
msgid "All Projects"
msgstr "Wszystkie projekty"

#: include/menus.php:75 index.php:168
msgid "Contribute"
msgstr "Wspomóż"

#: include/menus.php:77
msgid "Getting started"
msgstr "Pierwsze kroki"

#: include/menus.php:78 include/menus.php:96
msgid "Donate"
msgstr "Wesprzyj"

#: include/menus.php:79
msgid "Report a bug"
msgstr "Zgłoś błąd"

#: include/menus.php:83
msgid "Support"
msgstr "Wsparcie"

#: include/footer.php:33
msgid "Skins"
msgstr "Skóry"

#: include/footer.php:34
msgid "Extensions"
msgstr "Rozszerzenia"

#: include/footer.php:36 vlc/index.php:83
msgid "Screenshots"
msgstr "Zrzuty ekranu"

#: include/footer.php:61
msgid "Community"
msgstr "Społeczność"

#: include/footer.php:64
msgid "Forums"
msgstr "Forum"

#: include/footer.php:65
msgid "Mailing-Lists"
msgstr "Listy mailingowe"

#: include/footer.php:66
msgid "FAQ"
msgstr "Najczęściej zadawane pytania"

#: include/footer.php:67
msgid "Donate money"
msgstr "Wspomóż finansowo"

#: include/footer.php:68
msgid "Donate time"
msgstr "Poświęć czas"

#: include/footer.php:75
msgid "Project and Organization"
msgstr "Projekt i Organizacja"

#: include/footer.php:76
msgid "Team"
msgstr "Zespół"

#: include/footer.php:80
msgid "Mirrors"
msgstr "Serwery lustrzane"

#: include/footer.php:83
msgid "Security center"
msgstr "Centrum bezpieczeństwa"

#: include/footer.php:84
msgid "Get Involved"
msgstr "Zaangażuj się"

#: include/footer.php:85
msgid "News"
msgstr "Aktualności"

#: include/os-specific.php:103
msgid "Download VLC"
msgstr "Pobierz VLC"

#: include/os-specific.php:109 include/os-specific.php:295 vlc/index.php:168
msgid "Other Systems"
msgstr "Inne systemy"

#: include/os-specific.php:260
msgid "downloads so far"
msgstr "dotychczas pobrane pliki"

#: include/os-specific.php:648
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and "
"various streaming protocols."
msgstr ""
"VLC jest darmowym i otwartym, wieloplatformowym odtwarzaczem multimedialnym "
"i frameworkiem, który odtwarza większość plików multimedialnych oraz płyty "
"DVD, Audio CD, VCD i różnego rodzaju strumienie."

#: include/os-specific.php:652
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files, and various streaming protocols."
msgstr ""
"VLC to darmowy i otwarty, wieloplatformowy odtwarzacz multimedialny i także "
"framework, który odtwarza większość plików multimedialnych i różnego rodzaju "
"strumienie."

#: index.php:4
msgid "VLC: Official site - Free multimedia solutions for all OS!"
msgstr ""
"VLC: oficjalna strona - wieloplatformowe darmowe rozwiązania multimedialne!"

#: index.php:26
msgid "Other projects from VideoLAN"
msgstr "Inne projekty VideoLAN"

#: index.php:30
msgid "For Everyone"
msgstr "Dla każdego"

#: index.php:40
msgid ""
"VLC is a powerful media player playing most of the media codecs and video "
"formats out there."
msgstr ""
"VLC jest funkcjonalnym odtwarzaczem multimedialnym, obsługującym większość "
"istniejących kodeków i formatów wideo."

#: index.php:53
msgid ""
"VideoLAN Movie Creator is a non-linear editing software for video creation."
msgstr ""
"VideoLAN Movie Creator jest nieliniowym programem do tworzenia plików wideo."

#: index.php:62
msgid "For Professionals"
msgstr "Dla profesjonalistów"

#: index.php:72
msgid ""
"DVBlast is a simple and powerful MPEG-2/TS demux and streaming application."
msgstr ""
"DVBlast jest prostym i posiadającym duże możliwości dekoderem i aplikacją "
"strumieniową."

#: index.php:82
msgid ""
"multicat is a set of tools designed to easily and efficiently manipulate "
"multicast streams and TS."
msgstr ""
"multicat jest zestawem narzędzi przeznaczonych do łatwego i skutecznego "
"manipulowania strumieni multiemisji i TS."

#: index.php:95
msgid ""
"x264 is a free application for encoding video streams into the H.264/MPEG-4 "
"AVC format."
msgstr ""
"x264 jest darmową aplikacją, służącą do kodowania strumieni wideo na format "
"H.264/MPEG-4 AVC."

#: index.php:104
msgid "For Developers"
msgstr "Dla deweloperów"

#: index.php:140
msgid "View All Projects"
msgstr "Pokaż wszystkie projekty"

#: index.php:144
msgid "Help us out!"
msgstr "Pomóż nam!"

#: index.php:148
msgid "donate"
msgstr "wesprzyj"

#: index.php:156
msgid "VideoLAN is a non-profit organization."
msgstr "VideoLAN jest organizacją non-profit."

#: index.php:157
msgid ""
" All our costs are met by donations we receive from our users. If you enjoy "
"using a VideoLAN product, please donate to support us."
msgstr ""
"Wszystkie koszty pokrywane są z darowizn, które otrzymujemy od naszych "
"użytkowników. Jeśli używasz produktów VideoLAN z uśmiechem na twarzy i "
"chcesz nam pomóc, wesprzyj nas."

#: index.php:160 index.php:180 index.php:198
msgid "Learn More"
msgstr "Dowiedz się więcej"

#: index.php:176
msgid "VideoLAN is open-source software."
msgstr "VideoLAN jest otwartym oprogramowaniem."

#: index.php:177
msgid ""
"This means that if you have the skill and the desire to improve one of our "
"products, your contributions are welcome"
msgstr ""
"Oznacza to, że jeżeli posiadasz umiejętność i chęci, aby poprawić nasze "
"produkty, przyjmiemy Cię z otwartymi ramionami."

#: index.php:187
msgid "Spread the Word"
msgstr "Promuj nas"

#: index.php:195
msgid ""
"We feel that VideoLAN has the best video software available at the best "
"price: free. If you agree please help spread the word about our software."
msgstr ""
"Uważamy, że VideoLAN ma najlepsze oprogramowanie wideo dostępne w najlepszej "
"cenie: za darmo. Jeśli zgadzasz się z naszą opinią, wspomóż nas w promocji "
"naszych produktów."

#: index.php:215
msgid "News &amp; Updates"
msgstr "Nowości i aktualności"

#: index.php:218
msgid "More News"
msgstr "Więcej nowości"

#: index.php:222
msgid "Development Blogs"
msgstr "Blogi deweloperów"

#: index.php:251
msgid "Social media"
msgstr "Serwisy społecznościowe"

#: vlc/index.php:3
msgid "Official download of VLC media player, the best Open Source player"
msgstr ""
"Pobierz oficjalną wersję VLC media playera, najlepszego odtwarzacza Open "
"Source"

#: vlc/index.php:21
msgid "Get VLC for"
msgstr "Pobierz VLC dla"

#: vlc/index.php:29 vlc/index.php:32
msgid "Simple, fast and powerful"
msgstr "Prosty, szybki i wydajny"

#: vlc/index.php:35
msgid "Plays everything"
msgstr "Odtwarza wszystko"

#: vlc/index.php:35
msgid "Files, Discs, Webcams, Devices and Streams."
msgstr "pliki, płyty, kamery internetowe, urządzenia i strumienie."

#: vlc/index.php:38
msgid "Plays most codecs with no codec packs needed"
msgstr "Odtwarza większość kodeków bez potrzebnych pakietów"

#: vlc/index.php:41
msgid "Runs on all platforms"
msgstr "Działa na wszystkich platformach"

#: vlc/index.php:44
msgid "Completely Free"
msgstr "Całkowicie darmowy"

#: vlc/index.php:44
msgid "no spyware, no ads and no user tracking."
msgstr ""
"bez oprogramowania szpiegującego, bez reklam i bez śledzenia użytkownika."

#: vlc/index.php:47
msgid "learn more"
msgstr "dowiedz się więcej"

#: vlc/index.php:66
msgid "Add"
msgstr "Dodaj"

#: vlc/index.php:66
msgid "skins"
msgstr "skóry"

#: vlc/index.php:69
msgid "Create skins with"
msgstr "Utwórz skóry z"

#: vlc/index.php:69
msgid "VLC skin editor"
msgstr "edytorem skór VLC"

#: vlc/index.php:72
msgid "Install"
msgstr "Zainstaluj"

#: vlc/index.php:72
msgid "extensions"
msgstr "rozszerzenia"

#: vlc/index.php:126
msgid "View all screenshots"
msgstr "Wszystkie zrzuty ekranu"

#: vlc/index.php:135
msgid "Official Downloads of VLC media player"
msgstr "Oficjalne wydania odtwarzacza VLC"

#: vlc/index.php:146
msgid "Sources"
msgstr "Źródła"

#: vlc/index.php:147
msgid "You can also directly get the"
msgstr "Możesz również bezpośrednio pobrać"

#: vlc/index.php:148
msgid "source code"
msgstr "kod źródłowy"

#~ msgid "Can do media conversion and streaming."
#~ msgstr "Potrafi konwertować multimedia i transmisje strumieniowe."

#~ msgid "Discover all features"
#~ msgstr "Odkryj wszystkie możliwości"

#~ msgid "A project and a"
#~ msgstr "Projekt i"

#~ msgid ""
#~ "composed of volunteers, developing and promoting free, open-source "
#~ "multimedia solutions."
#~ msgstr ""
#~ "złożona z ochotników, produkująca i promująca darmowe, otwarte "
#~ "rozwiązania multimedialne."

#~ msgid "why?"
#~ msgstr "dlaczego?"

#~ msgid "Home"
#~ msgstr "Strona główna"

#~ msgid "Support center"
#~ msgstr "Centrum pomocy"

#~ msgid "Dev' Zone"
#~ msgstr "Strefa dewelopera"

#~ msgid "DONATE"
#~ msgstr "WESPRZYJ"

#~ msgid "Other Systems and Versions"
#~ msgstr "Inne systemy i wersje"

#~ msgid "Other OS"
#~ msgstr "Inne systemy operacyjne"
